# Json Users Import

This module used to import users from json data without uploading any files.
Normally we have CSV file to import the users while migrating or moving the
resource form other portals.

Sometimes we got the users data in the JSON format. In that time our Json Users
Import helps to import the users data without any data loss.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/json_users_import).

- To submit bug reports and feature suggestions, or track changes
  [issue queue](https://www.drupal.org/project/issues/json_users_import).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. Visit
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.


## Configuration

- Click the "Configuration" menu from admin toolbar.
- Then click the "People" menu and then "Json users import configuration" menu.
- Now you will have the Json users import configuration page. And all the
  core and custom fields from the User accounts will be listed here.
- Map the fields name with the json key from the json data you have.
- If you want to send mail to the user about the new account created,
  select the "Send E-Mail to users" option and fill the Subject, E-Mail
  option and Content of the E-Mail.
- We have two option here
    - We can Send One time login URL (or)
    - We can Send Password
- Select the option you want and click the Save button!


## Importing users

- Click the People menu from admin toolbar.
- Then click the Json users import tab menu.
- In the User Import Configuration, map the fields in the User accounts
  with the json key.
- Paste the Json data in the Paste your json here text area.
- Finally, click the Import button!


## Fields

The following fields are the standard fields and they always mandatory for
the import.

- username: (required) Username of the user
- email: (required) Email of the user

We can add custom fields also which is created in user account
`/admin/config/people/accounts/fields` and map the key as per your need.


## Maintainers

- Kurinjiselvan Venugopal - [kurinjiselvan v](https://www.drupal.org/u/kurinjiselvan-v)
