<?php

namespace Drupal\json_users_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\field\FieldConfigInterface;
use Drupal\Component\Utility\Random;

/**
 * Controller for Json users import.
 */
class JsonUsersImportController extends ControllerBase {

  /**
   * Returns the fields machine name from the user entity.
   *
   * @return array
   *   User fields machine name.
   */
  public function getUserFields() {
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $userAccountFields = array_filter($field_definitions, function ($fieldDefinition) {
      return $fieldDefinition instanceof FieldConfigInterface;
    }
    );
    return $userAccountFields;
  }

  /**
   * Returns user data if email id is exists.
   *
   * @param string $email
   *   Email id from the currently uploaded json data.
   *
   * @return array
   *   User Id and Error message.
   */
  public function isEmailExist($email) {
    $userId = \Drupal::entityQuery('user')->accessCheck(TRUE)->condition('mail', $email)->execute();
    $userId = array_shift($userId);
    if ($userId > 0) {
      return ['id' => $userId, 'returnMsg' => $email . ' is already exists!'];
    }
  }

  /**
   * Returns user data if username is exists.
   *
   * @param string $username
   *   Username from the currently uploaded json data.
   *
   * @return array
   *   User Id and Error message.
   */
  public function isUserNameExist($username) {
    $userId = \Drupal::entityQuery('user')->accessCheck(TRUE)->condition('name', $username)->range(0, 1)->execute();
    $userId = array_shift($userId);
    if ($userId > 0) {
      return ['id' => $userId, 'returnMsg' => $username . ' is already exists!'];
    }
  }

  /**
   * Returns array if username is valid.
   *
   * @param string $username
   *   Username from the currently uploaded json data.
   *
   * @return array
   *   Return FALSE, if username is valid and Error message.
   *   Otherwise return TRUE.
   */
  public function isUserNameValid($username) {
    $valid = TRUE;
    $returnMsg = '';
    if (substr($username, 0, 1) == ' ') {
      $valid = FALSE;
      $returnMsg = $username . ' cannot begin with a space.';
    }
    elseif (substr($username, -1) == ' ') {
      $valid = FALSE;
      $returnMsg = $username . ' cannot end with a space.';
    }
    elseif (strpos($username, '  ') !== FALSE) {
      $valid = FALSE;
      $returnMsg = $username . ' cannot contain multiple spaces.';
    }
    elseif (preg_match('/[^\x{80}-\x{F7} a-z0-9@+_.\'-]/i', $username)) {
      $valid = FALSE;
      $returnMsg = $username . ' contains an illegal character.';
    }
    elseif (mb_strlen($username) > 60) {
      $valid = FALSE;
      $returnMsg = $username . ' is too long, it must be 60 characters or less.';
    }
    return ['valid' => $valid, 'returnMsg' => $returnMsg];
  }

  /**
   * Create user data if user not exists.
   *
   * @param array $value
   *   An array of user data from the currently uploaded json data.
   * @param string $emailKey
   *   Email key from the currently uploaded json data.
   * @param string $usernameKey
   *   Username key from the currently uploaded json data.
   * @param array $fields
   *   An array of fields from the user configuration.
   *
   * @return string
   *   Status of the created user record.
   */
  public function createUser(array $value, $emailKey, $usernameKey, array $fields) {
    $config = \Drupal::config('json_users_import.import_configuration');
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $statusMsg = '';
    $random = new Random();
    $password = $random->name(7);
    // Creating the user account.
    $user = User::create();
    $user->setEmail($value[$emailKey]);
    $user->setUsername($value[$usernameKey]);
    $user->setPassword($password);
    $user->enforceIsNew();
    $user->set('init', $value[$emailKey]);
    $user->set('langcode', $language);
    $user->set('preferred_langcode', $language);
    $user->set('preferred_admin_langcode', $language);
    $user->activate();
    $ignoreFields = ["email", "name", "password"];
    foreach ($fields as $fieldName => $fieldValue) {
      if (!in_array($fieldName, $ignoreFields)) {
        if (isset($value[$config->get($fieldName)])) {
          $user->set($fieldName, $value[$config->get($fieldName)]);
        }
      }
    }
    // Save user account.
    $user->save();
    if ($user->isActive()) {
      $userId = $user->id();
      // Send mail if the mail configuration is enabled.
      if ($config->get('send_email')) {
        $this->sendUserAccountDetails($value[$emailKey], $userId, $password);
      }
      $statusMsg = 'User ' . $value[$usernameKey] . ' is created successfully!';
      \Drupal::logger('json_users_import')->notice($statusMsg);
    }
    else {
      $statusMsg = 'User ' . $value[$usernameKey] . ' is not created!';
      \Drupal::logger('json_users_import')->error($statusMsg);
    }
    return ['statusMsg' => $statusMsg, 'userId' => $userId];
  }

  /**
   * Send the user credentials or one time password link.
   *
   * @param string $email
   *   An array of user data from the currently uploaded json data.
   * @param string $userId
   *   Email key from the currently uploaded json data.
   * @param string $password
   *   Username key from the currently uploaded json data.
   */
  public function sendUserAccountDetails($email, $userId, $password) {
    $config = \Drupal::config('json_users_import.import_configuration');
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'json_users_import';
    $key = 'user_created';
    $to = $email;
    if ($config->get('email_options') == 'send_onetime_login') {
      $params['message'] = $config->get('onetime_login_content');
      $params['password'] = '';
    }
    else {
      $params['message'] = $config->get('password_content');
      $params['password'] = $password;
    }
    $params['email_options'] = $config->get('email_options');
    $params['title'] = $this->t('New account creation');
    $params['userId'] = $userId;
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] != TRUE) {
      $message = $this->t('There was a problem sending your email notification to @email.', ['@email' => $to]);
      \Drupal::messenger()->addError($message);
      \Drupal::logger('mail-log')->error($message);
      return;
    }
    $message = $this->t('An email notification has been sent to @email', ['@email' => $to]);
    \Drupal::messenger()->addStatus($message);
    \Drupal::logger('mail-log')->notice($message);

  }

}
