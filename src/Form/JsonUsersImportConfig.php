<?php

namespace Drupal\json_users_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\json_users_import\Controller\JsonUsersImportController;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to save the json users import configuration.
 */
class JsonUsersImportConfig extends ConfigFormBase {

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Json Users Import object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger object.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId() {
    return 'json_users_import_configform';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['json_users_import.import_configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('json_users_import.import_configuration');

    $jsonUsersImportController = new JsonUsersImportController();
    $fields = $jsonUsersImportController->getUserFields();

    $form['user_import_fieldmap'] = [
      '#type' => 'details',
      '#title' => $this->t('User Import Field Configuration'),
      '#attributes' => ['id' => ['user_import_fieldmap']],
      '#open' => TRUE,
    ];
    $form['user_import_fieldmap']['additional_fields'] = [
      '#type' => 'hidden',
      '#value' => array_keys($fields),
    ];
    $form['user_import_fieldmap']['filed_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('filed_email:'),
      '#default_value' => $config->get('filed_email'),
      '#required' => TRUE,
    ];
    $form['user_import_fieldmap']['filed_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('filed_name:'),
      '#default_value' => $config->get('filed_name'),
      '#required' => TRUE,
    ];
    foreach ($fields as $fieldname => $fieldvalues) {
      $form['user_import_fieldmap'][$fieldname] = [
        '#type' => 'textfield',
        '#title' => ($fieldname . ':'),
        '#default_value' => $config->get($fieldname),
      ];
    }
    $form['user_import_email_config'] = [
      '#type' => 'details',
      '#title' => $this->t('E-Mail Configuration'),
      '#attributes' => ['id' => ['user_import_email_config']],
      '#open' => TRUE,
    ];
    $form['user_import_email_config']['send_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send E-Mail to users'),
      '#default_value' => $config->get('send_email'),
      '#required' => FALSE,
      '#description' => '<p>' . $this->t('Users will received email once user record is created successfully!.') . '</p>',
    ];
    $form['user_import_email_config']['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject:'),
      '#default_value' => $config->get('email_subject'),
      '#states' => [
        'visible' => [
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
      ],
      '#token_types' => ['user'],
      '#show_restricted' => TRUE,
    ];
    $options = [
      'send_onetime_login' => $this->t('Send One time login URL'),
      'send_password' => $this->t('Send Password'),
    ];
    $form['user_import_email_config']['email_options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select the E-Mail option'),
      '#options' => $options,
      '#default_value' => ($config->get('email_options')) ? $config->get('email_options') : 'send_onetime_login',
      '#states' => [
        'visible' =>
        [
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
        'required' =>
        [
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['user_import_email_config']['onetime_login_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Email Welcome Content:"),
      '#description' => '<p>' . $this->t('Edit the welcome email messages sent to new member accounts created by an administrator. Available variables are:') . $this->t('[site:name], [site:url], [user:display-name], [user:account-name], [user:mail], [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url].') . '</p>',
      '#default_value' => $config->get('onetime_login_content'),
      '#states' => [
        'visible' =>
        [
          ':input[name="email_options"]' => ['value' => 'send_onetime_login'],
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
        'required' =>
        [
          ':input[name="email_options"]' => ['value' => 'send_onetime_login'],
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
      ],
      '#token_types' => ['user'],
    ];
    $form['user_import_email_config']['password_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Email Welcome Content:"),
      '#description' => '<p>' . $this->t('Edit the welcome email messages sent to new member accounts created by an administrator. Available variables are:') . ('[site:name], [site:url], [user:display-name], [user:account-name], [user:mail], [site:login-url], [site:url-brief], [user-password].') . '</p>',
      '#default_value' => $config->get('password_content'),
      '#states' => [
        'visible' =>
        [
          ':input[name="email_options"]' => ['value' => 'send_password'],
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
        'required' =>
        [
          ':input[name="email_options"]' => ['value' => 'send_password'],
          ':input[name="send_email"]' => ['checked' => TRUE],
        ],
      ],
      '#token_types' => ['user'],
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('json_users_import.import_configuration');
    $config->set('filed_email', $values['filed_email'])->save();
    $config->set('filed_name', $values['filed_name'])->save();
    $config->set('send_email', $values['send_email'])->save();
    $config->set('email_subject', $values['email_subject'])->save();
    $config->set('email_options', $values['email_options'])->save();
    $config->set('onetime_login_content', $values['onetime_login_content'])->save();
    $config->set('password_content', $values['password_content'])->save();
    if ($values['additional_fields']) {
      foreach ($values['additional_fields'] as $additionalFields) {
        $config->set($additionalFields, $values[$additionalFields])->save();
      }
    }
    $this->messenger->addMessage($this->t('Successfully saved!'));
  }

}
