<?php

namespace Drupal\json_users_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to import the users from Json.
 */
class JsonUsersImport extends FormBase {

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Json Users Import object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger object.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId() {
    return 'json_users_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['user_import'] = [
      '#title' => $this->t('Paste your json here'),
      '#type' => 'textarea',
      '#rows' => 24,
      '#required' => TRUE,
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $users_json = $form_state->getValue('user_import');
    $json_decoded = Json::decode($users_json);
    if (is_array($json_decoded)) {
      $operations = [
        ['json_users_import_creating_users_batch', [$json_decoded]],
      ];
      $batch = [
        'title' => $this->t('Creating Users...'),
        'operations' => $operations,
        'finished' => 'json_users_import_users_finished_batch',
      ];
      batch_set($batch);
    }
    else {
      $this->messenger->addMessage("Not a valid Json!");
    }

  }

}
